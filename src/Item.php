<?php
namespace Maksoft\Cart;
use Maksoft\Amount\Money;
bcscale(2);


class Item
{
    private $price;
    private $qty;
    private $sku;
    private $unit;
    private $name;
    private $category;
    private $moq=1;
    private $total=0;
    private $data = array();
    private $branding = false;

    public function __construct($sku, $qty, $price, $name, $unit, $category='')
    {
        $this->setSku($sku);
        $this->setQty($qty);
        $this->setPrice($price);
        $this->setName($name);
        $this->setUnit($unit);
        $this->setCategory($category);
        $this->total();
    }

    public function __toString()
    {
        return $this->getPrice();
    }

    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }
        return null;
    }

    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    public function __unset($name)
    {
        unset($this->data[$name]);
    }

    public function setMoq($pcs)
    {
        $this->moq = $pcs;
        return $this;
    }

    public function getMoq()
    {
        return $this->moq;
    }

    public function setCategory($category)
    {
        $this->category = trim($category);
        return $this;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function withBranding($default=True)
    {
        $this->branding = $default;
    }

    public function toArray()
    {
        $data = array(
            'sku' => $this->getSku(), 
            'price' => $this->getPrice(),
            'qty' => $this->getQty(),
            'name' => $this->getName(),
            'unit' => $this->getUnit(),
            'branding' => $this->branding,
            'total' => $this->total(),
            'category' => $this->getCategory(),
            'moq' => $this->getMoq(),
            'attributes' => $this->data,
        );

        return $data;
    }


    public function setUnit($unit)
    {
        $this->unit = trim($unit);
        return $this;
    }

    public function getUnit()
    {
        return $this->unit;
    }

    public function setName($name)
    {
        $this->name = trim($name);
        return $this;
    }
    public function getName()
    {
        return $this->name;
    }

    public function setQty($qty)
    {
        $this->qty = (string) ($qty < 1 ? 0 : $qty);
        return $this;
    }

    public function getQty()
    {
        return $this->qty;
    }

    public function setPrice($price)
    {
        $this->price = trim($price);
        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setSku($sku)
    {
        $this->sku = trim($sku);
        return $this;
    }

    public function getSku()
    {
        return $this->sku;
    }


    public function add(Item $other_item)
    {
        if(!$this->eq($other_item)){
            return False;
        }
        $this->setQty(bcadd($this->getQty(), $other_item->getQty()));

        $this->total();

        return True;
    }

    public function total()
    {
        $this->total = bcmul($this->getQty(), $this->getPrice());
        return $this->total;
    }

    public function sub(Item $other_item)
    {
        if(!$this->eq($other_item)){
            return False;
        }
        $this->setQty($this->getQty() - $other_item->getQty());
        return True;
    }

    public function eq(Item $other_item)
    {
        if($this->getSku() === $other_item->getSku() && $this->getAttributes() == $other_item->getAttributes()){
            return True;
        }
        return False;
    }

    public function hash()
    {
        return spl_object_hash($this);
    }

    public function getAttributes()
    {
        return $this->data;
    }

    public function sum($currency)
    {
        $money = Money::$currency($this->getPrice());
        $money->mul($this->getQty());
        return $money->getAmount();
    }

    public function load(array $item_data)
    {
        $this->data = $item_data;
    }

    public function save()
    {
        return json_encode($this->data);
    }

    public function reset()
    {
        unset($_SESSION[Cart::getName()]);
    }
}
