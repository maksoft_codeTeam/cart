<?php
namespace Maksoft\Cart;


class Factory
{
	private static $currency = array("BG", "EUR", "USD");
    private static $instance;

	public static function init($valuta="BG")
	{
		if(empty($_SESSION[Cart::getName()])){
            self::factory($valuta);
            return self::$instance;
		}
        $items = $_SESSION[Cart::getName()];
		self::factory($valuta);
        foreach($items as $item) {
            self::$instance->add(new Item($item));
        }
        return self::$instance;
	}

    public static function start_session()
    {
		if(session_id()=="") {
			session_start();
		}
    }

    private static function factory($valuta)
    {
        self::start_session();
        if(!self::$instance){
            self::$instance = new Cart($valuta);
        }
        return self::$instance;
    }

	public static function add(Item $item)
	{
        self::$instance->add($item);
	}

	public static function sum()
	{
        return self::$instance->sum();
	}

	public static function remove(Item $item)
	{
        return self::$instance->remove($item);
	}

	public static function edit(Item $item)
	{
        return self::$instance->edit($item);
	}

	public static function save()
	{
        self::$instance->save();
	}

	public static function toJSON()
	{
        return self::$instance->toJSON();
	}

	public static function count()
	{
        return self::$instance->count();
	}

    public static function items()
    {
        return self::$instance->get_items();
    }
}
?>
