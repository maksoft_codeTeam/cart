<?php
namespace Maksoft\Cart;
use Maksoft\Amount\Money;


class Cart
{
    protected static $cart_name='cart';

    protected $items=array();

    protected $vat="1.2";
    protected $currency = array("BG", "EUR", "USD");

    public function __construct($currency){
        register_shutdown_function(array($this, '__destruct'));
        if(in_array($currency, $this->currency)){
            $this->currency = $currency;
            return;
        }
        throw new \Exception("Init method has 1 argument currency. Available currencies are BG, EUR, USD", 402);
    }

    public static function setName($name)
    {
        self::$cart_name = $name;
    }

    public static function getName()
    {
        return self::$cart_name;
    }

    public function getCurrencySymbol()
    {
        $money = Money::$this->currency($this->sum());
        return $money->getCurrencySymbol();
    }

    public function add(Item $item)
    {
        foreach($this->items as $i){
            if($i->add($item)){
                return;
            }
        }
        $this->items[$item->hash()] = $item;
    }

    public function sum()
    {
        $sum = 0;
        foreach($this->items as $item){
            $sum = bcadd("$sum", $item->sum($this->currency));
        }
        if($sum >0){
            return $sum;
        }
        return 0;
    }

    public function skus()
    {
        $t = array();
        foreach($this->items as $item){
            $t[] = array(
                'sku'=>$item->hash(),
                "qty"=>$item->getQty(),
            );
        }
        return $t;
    }

    public function sum_with_vat()
    {
        return bcmul($this->vat, $this->sum());
    }

    public function vat()
    {
        return bcsub($this->sum_with_vat(), $this->sum());
    }

    public function remove(Item $item)
    {
        if(array_key_exists($item->hash(), $this->items)){
            unset($this->items[$item->hash()]);
            return True;
        }
        throw new \Exception("Cant Remove Item! Item doesnt exist!", 400);
    }

    public function edit(Item $item)
    {
        if(array_key_exists($item->hash(), $this->items)){
            $this->items[$item->hash()] = $item;
            return True;
        }
        throw new \Exception("Cant Edit Item that doesnt exist!", 400);
    }

    public function save()
    {
        $tmp = array();
        foreach($this->items as $item){
            $tmp[] = $item->toArray();
        }
        return $tmp;
    }

    public function load(array $items)
    {
        $this->items = array();
        if(empty($items)){
            return;
        }
        foreach($items as $item){
            if(!array_key_exists('category', $item)){
                $item['category'] = '';
            }
            $it = new Item($item['sku'], $item['qty'], $item['price'], $item['name'], $item['unit'], $item['category']);
            if(array_key_exists('moq', $item)){
                $it->setMoq($item['moq']);
            }

            if(array_key_exists('attributes', $item)){
                foreach($item['attributes'] as $k=>$v){
                    $it->$k = $v;
                }
            }
            $this->add( $it );
        }
    }

    public function toJSON()
    {
        throw new \Exception("Total sum of cart cant be a negative number", 401);
        return json_encode($this->items);
    }

    public function count()
    {
        return count($this->items);
    }

    public function get_items()
    {
        $data = array();
        foreach ($this->items as $item){
            $data[] = $item->toArray();
        }
        return $data;
    }

    public function get_by_sku($sku)
    {
        if(array_key_exists($sku, $this->items)){
            return $this->items[$sku];
        }
        foreach($this->items as $item){
            if($item->getSku() == $sku){
                return $item;
            }
        }
        return;
    }

    public function get_objects()
    {
        return $this->items;
    }

    public function reset()
    {
        $this->items = array();
        unset($_SESSION[self::$cart_name]);
    }
    public function __destruct()
    {
        $_SESSION[self::$cart_name] = $this->save();
    }
}

