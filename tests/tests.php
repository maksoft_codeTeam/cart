<?php
require __DIR__ . "/../vendor/autoload.php";
use Maksoft\Cart\Cart;
use Maksoft\Cart\Factory;
use Maksoft\Cart\Item;


class TestCart extends PHPUnit_Framework_TestCase
{
	public function setUp()
	{
        $_SESSION = array();
		$this->items = array( array(
				'itID' => '42372',
				'qty_on_stock' => '0',
				'price' => '0.37',
				'itPeriod' => '3',
				'commision' => '10',
				'discount' => '1',
				'slType' => '0',
				'prior' => '1',
				'price_id' => '484044',
				'price_n' => '182392',
				'price_SiteID' => '2',
				'price_description' => 'химикалка БОРДО сребърен връх, без печат',
				'price_code' => '42372',
				'price_pcs' => '1',
				'price_qty' => '1',
				'price_value' => '0.38',
				'price_currency' => '0',
				'price_changed' => '0000-00-00 00:00:00',
                'PHPcode' => 'echo "123456";',
                'm' => 'бр',
                'PHPvars' => 'echo "123456";'
			) ,
			array(
				'itID' => '10221',
				'qty_on_stock' => '0',
				'price' => '0.21',
				'itPeriod' => '3',
                'm' => 'бр',
				'commision' => '8',
				'discount' => '1',
				'slType' => '1',
				'prior' => '1',
				'price_id' => '484361',
				'price_n' => '128376',
				'price_SiteID' => '2',
				'price_description' => 'химикалка туист синя',
				'price_code' => '10221',
				'price_pcs' => '1',
				'price_qty' => '1',
				'price_value' => '0.21',
				'price_currency' => '2',
				'price_changed' => '0000-00-00 00:00:00',
			));
        $this->cart = new Cart("BG");
        $it1 = $this->items[0];
        $this->item = new Item($it1['itID'], $it1['price_pcs'], $it1['price_value'], $it1['price_description'], $it1['m']);
	}

	public function test_add_to_cart()
	{
        $it1 = $this->items[0];
        $item = new Item($it1['itID'], $it1['price_pcs'], $it1['price_value'],  $it1['price_description'], $it1['m']);
		$this->cart->add($item);
        $this->assertEquals(1, $this->cart->count(), 
            "Тест:  добавяне на  1 продукт"
        );

        $it1 = $this->items[1];
        $other_item = new Item($it1['itID'], $it1['price_pcs'], $it1['price_value'],  $it1['price_description'], $it1['m']);
		$this->cart->add($other_item);
        $this->assertEquals(2, $this->cart->count(), 
            "Тест:  добавяне на  2 продуктa"
        );
	}

	public function test_remove_item_that_exist_in_cart()
	{
        $it1 = $this->items[1];
        $other_item = new Item($it1['itID'], $it1['price_pcs'], $it1['price_value'],  $it1['price_description'], $it1['m']);

        $it1 = $this->items[0];
        $item = new Item($it1['itID'], $it1['price_pcs'], $it1['price_value'],  $it1['price_description'], $it1['m']);
		$this->cart->add($item);
        $this->assertTrue($this->cart->remove($item),
            "Тест: remove() връща True когато продукта го има в кошницата"
        );
		$code = 0;
		try{
			$this->cart->remove($other_item);
		} catch (Exception $e) {
			$code = $e->getCode();
		}
        $this->assertEquals(400, $code, 
            "Тест: очаквана грешка с код 400, когато се премахва продукт, който го няма в кошницата"
        );
	}

	public function test_sum_cart()
	{
        $it1 = $this->items[1];
        $other_item = new Item($it1['itID'], $it1['price_pcs'], $it1['price_value'],  $it1['price_description'], $it1['m']);

        $it1 = $this->items[0];
        $item = new Item($it1['itID'], $it1['price_pcs'], $it1['price_value'],  $it1['price_description'], $it1['m']);

		$this->cart->add($item);
		$this->cart->add($other_item);
		$this->cart->add($other_item);
		$this->cart->add($item);
        $this->assertEquals(1.18, $this->cart->sum(), 
            "Тест: тества се сумирането на кошницата. Очаквана стойност 1.18"
        );
	}

	public function test_sum_result_is_negative_number()
	{
		$code = 0;
		$it1 = $this->items[0];
		$it1["price_value"] = -100.013;
        $item1 = new Item($it1['itID'], $it1['price_pcs'], $it1['price_value'],  $it1['price_description'], $it1['m']);

        $it1 = $this->items[1];
        $item2 = new Item($it1['itID'], $it1['price_pcs'], $it1['price_value'],  $it1['price_description'], $it1['m']);

        $this->cart->add($item1);
        $this->cart->add($item2);
        $this->assertEquals(0, $this->cart->sum(),
            "Тест: сумата на всички продукти не трябва да е по-малка от 0. Очаквана стойност 0"
        );
	}

    public function test_add_sameSkuItems_ExpectItemQty1()
    {
        $_SESSUION = array();
        $data = array(
				'itID' => '42372',
				'qty_on_stock' => '0',
				'price' => '0.37',
				'itPeriod' => '3',
				'commision' => '10',
				'discount' => '1',
				'slType' => '0',
				'prior' => '1',
				'price_id' => '484044',
				'price_n' => '182392',
                'm' => 'бр.',
				'price_SiteID' => '2',
				'price_description' => 'химикалка БОРДО сребърен връх, без печат',
				'price_code' => '42372',
				'price_pcs' => '1',
				'price_qty' => '1',
				'price_value' => '0.38',
				'price_currency' => '0',
				'price_changed' => '0000-00-00 00:00:00',
        );
        $this->cart->add(new Item($data['itID'], $data['price_pcs'], $data['price_value'], $data['price_description'], $data['m']));
        $this->cart->add(new Item($data['itID'], $data['price_pcs'], $data['price_value'], $data['price_description'], $data['m']));
        $this->cart->add(new Item($data['itID'], $data['price_pcs'], $data['price_value'], $data['price_description'], $data['m']));
        $this->cart->add(new Item($data['itID'], $data['price_pcs'], $data['price_value'], $data['price_description'], $data['m']));
        $this->cart->add(new Item($data['itID'], $data['price_pcs'], $data['price_value'], $data['price_description'], $data['m']));
        $this->assertEquals(1, $this->cart->count(), 
            "Тест: добавяне на 5 еднакви продукта един след друг в кошницата, очакван резултат 1 продукт и к-во 5" 
        );
        $it1 = array_shift($this->cart->get_items());
        $item = new Item($it1['sku'], $it1['qty'], $it1['price'],  $it1['name'], $it1['unit']);
        $this->assertEquals(5, $item->getQty(),
            "Тест: след добавяне на 5 еднакви продукта проверявам дали количеството е 5." 
        );
    }

	public function test_cart_load()
	{
        $cart = new Cart("BG");
        $items = array();
        $items[] = array('sku' => 12312, 'price' => '0.312', 'name' => 'adsasdas', 'unit' => 'бр', 'qty' => 13);
        $items[] = array('sku' => 123121, 'price' => '0.312', 'name' => 'adsasdas', 'unit' => 'бр', 'qty' => 13);
        $cart->load($items);
        $cart->get_items();
        $this->assertEquals(2, $cart->count(), 
            "Тества когато се изпълни метода load с array от два продукта, че броят им в действителност е 2");
        $this->assertEquals(2, count($cart->get_items()), 
            "Тества когато се изпълни метода load с array от два продукта, че броят им в действителност е 2");
	}

    public function test_Item()
    {
		$it1 = $this->items[0];
        $item1 = new Item($it1['itID'], $it1['price_pcs'], $it1['price_value'],  $it1['price_description'], $it1['m']);

        $it1 = $this->items[1];
        $item2 = new Item($it1['itID'], $it1['price_pcs'], $it1['price_value'],  $it1['price_description'], $it1['m']);

        $this->assertFalse( $this->item->add($item2),
            "Тест: събиране на два различни продукта. Очаквана стойност boolean(false)"
        );
        $this->assertFalse( $this->item->eq($item2),
            "Тест: Два различни продукта са еднакви. Очаквана стойност boolean(false)"
        );
        $this->assertFalse( $this->item->sub($item2),
            "Тест: изваждане на два различни продукта. Очаквана стойност boolean(false)"
        );

        $this->assertTrue( $this->item->add($item1),
            "Тест: събиране на два еднакви продукта. Очаквана стойност boolean(True)"
        );

        $this->assertEquals(2, $this->item->getQty(), 
            "Тест: количество след събиране на еднакви продукти. Очаквана стойност 2"
        );
        



    }

    public function test_item_add_with_another_item__equal()
    {

    }


    public function test_item_eq_with_another_item__equal()
    {

    }

    public function test_item_sub_with_another_item__equal()
    {

    }
}
